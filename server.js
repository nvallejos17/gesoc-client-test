const express = require('express')
const cors = require('cors')
const app = express()

// Enable CORS
app.use(cors())

// Static assets
app.use(express.static(`${__dirname}/build`))

const PORT = process.env.PORT || 5000

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`)
})
