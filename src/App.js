import React, { useState, useEffect } from 'react'
import axios from 'axios'

const App = () => {
  const [data, setData] = useState(null)

  useEffect(() => {
    axios
      .get('/api/egresos')
      .then((res) => {
        setData(res.data)
      })
      .catch((err) => console.error(err))
  }, [])

  return (
    <>
      <div>¡Bienvenido a GeSoc!</div>
      <div>
        {data
          ? `Conexión con Spark exitosa. Respuesta: ${data.Error}`
          : 'No se pudo conectar con Spark'}
      </div>
    </>
  )
}

export default App
